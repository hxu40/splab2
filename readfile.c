

#include <stdio.h>
#include <stdlib.h>

#include "readfile.h"
int read_float(float *f, FILE* data)
{
    int ret = fscanf(data, "%f", f);
    if (ret != 1) {
      return -1;
    }
    return 0;
}


int read_string(char *s, FILE* data)
{
    int ret = fscanf(data, "%s", s);
    if (ret != 1) {
      return -1;
    }
    return 0;
}

int read_int(int *d, FILE* data)
{
    
  int ret = fscanf(data, "%d", d);
    if (ret != 1) {
      return -1;
    }
    return 0;
}



int oepn_file(char *fileName, FILE **fp){
    
    *fp = fopen(fileName,"r");
    if(*fp == NULL){
        return -1;
    }
    else {
        return 0;
    }
}

void close_file(FILE **in_file){
    fclose(*in_file);
}