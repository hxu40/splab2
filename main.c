// arthor: Hua Xu
// date: 11-3-2021
// SP lab_1
// CS 402



#include <ctype.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "readfile.h"
#define MAXNAME  64
#define MAX_SIZE 1024
#include <stdlib.h>
#include<conio.h>
typedef struct{
    int ID;
    char fName[MAXNAME];
    char lName[MAXNAME];
    int salary;
}Employee;

Employee emp[MAX_SIZE];

int empCount = 0;

// the function for the qsort to compare the ID
int compare (const void * a, const void * b)
{

  Employee *orderA = (Employee *)a;
  Employee *orderB = (Employee *)b;

  return ( orderA->ID - orderB->ID );
}


// the fucntion for the qsort to get the salary in descend order
int compare_salary (const void * a, const void * b)
{

  Employee *orderA = (Employee *)a;
  Employee *orderB = (Employee *)b;

  return ( orderB->salary - orderA->salary );
}




// this function asks the user to input the Employee information, and check if they
// want to add this information to the user.
void addEmp(){
    Employee empy;
    printf("Enter the first name of the employee: ");
    scanf("%s",empy.fName);
    printf("Enter the last name of the employee: ");
    scanf("%s",empy.lName);
    printf("Enter employee's salary (30000 to 150000): ");
    scanf("%d",&empy.salary);
    // if the user salry is not correct
    while(empy.salary < 30000 || empy.salary > 150000){
        printf("Invlaid Salary\n");
        printf("Enter employee's salary (30000 to 150000): ");
        scanf("%d",&empy.salary);
    }
    int choice;
    printf("do you want to add the following employee to the DB?\n");
    printf("      %s", empy.fName);
    printf(" %s", empy.lName);
    printf(", salary: %d\n", empy.salary);
    printf("Enter 1 for yes, 0 for no: ");
    scanf("%d",&choice);
    // put the information into database
    if(choice == 1){
        empy.ID = emp[empCount-1].ID + 1;
       
        emp[empCount] = empy;
        empCount++;
    }
}

// this function require to the user to search the Employee with required last name,
// if we have the Employee with this last name, show the information of this Employee
// otherwise, show not found
void lookByLName(){
    char name[MAXNAME];
    printf("\nEnter Employee's last name (no extra spaces): ");
    scanf("%s",name);
        
    for(int i=0;i< empCount; i++){
        
        if(strcmp(emp[i].lName,name) == 0){
            printf("\n");
            printf("Fisrt_Name   Last_Name         SALARY             ID\n");
            printf("-----------------------------------------------------\n");
            printf("%s", emp[i].fName);
            printf("     \t%s", emp[i].lName);
            printf("     \t%d", emp[i].salary);
            printf("     \t%d\n", emp[i].ID);
            printf("-----------------------------------------------------\n");
            return;
        }
    }
    
    printf("Employee not found\n");
}

void lookByID(){
    int id1;
    printf("\nPlease enter employee ID: ");
    scanf("%d",&id1);
    for(int i=0;i< empCount; i++){
        // if find the employee
        if(emp[i].ID == id1){
          printf("\n");
          printf("Fisrt_Name   Last_Name         SALARY             ID\n");
          printf("-----------------------------------------------------\n");
          printf("%s", emp[i].fName);
          printf("     \t%s", emp[i].lName);
          printf("     \t%d", emp[i].salary);
          printf("     \t%d\n", emp[i].ID); 
          printf("-----------------------------------------------------\n");
          return;
        }
    }
    printf("Employee with id %d not found in DB\n", id1);
}

// this function prints out the total number of employees in the database in tabular
void printAll(){
    printf("\n");
    printf("Fisrt_Name   Last_Name         SALARY             ID\n");
    printf("-----------------------------------------------------\n");
    for(int i=0;i< empCount; i++){
    printf("%s", emp[i].fName);
    printf("     \t%s", emp[i].lName);
    printf("     \t%d", emp[i].salary);
    printf("     \t%d\n", emp[i].ID); 
    }
    printf("-----------------------------------------------------\n");
    printf("Number of Employees  (%d)\n", empCount);
}


// the format of output information
void format() {
    printf("Fisrt_Name   Last_Name    SALARY        ID\n");
    printf("-----------------------------------------------------\n");
    printf("%s", emp[1].fName);
    printf("         %s", emp[1].lName);
    printf("        %d", emp[1].salary);
    printf("        %d", emp[1].ID);
}

// this is the menu to guide the user how to handle the database
void menu() {
    printf("\nEmployee DB Menu:\n");
    printf("--------------------------\n");
    printf("(1) Print the Database\n");
    printf("(2) Lookup by ID\n");
    printf("(3) Lookup by Last Name\n");
    printf("(4) Add an Employee\n");
    printf("(5) Quit\n");
    printf("(6) Remove an employee\n");
    printf("(7) Update an employee's information.");
    printf("\n(8) Print the M employees with the highest salaries.");
    printf("\n(9) Find all employees with matching last name.\n");
    printf("--------------------------\n");
}




// according the user input, it generate the integer. If the input is
// not correct, it will ask user re_input again
int readChoice() {
    int choice;
     printf("Enter your choice: ");
     scanf("%d",&choice); 
     while( choice > 6 || choice < 0) {
        printf("Hey, %d is not between 1 and 5, try again...", choice);
        printf("Enter your choice: ");
        scanf("%d",&choice); 
     }
     return choice;
}


// the function to remove the employee without a hole in array
void removeEmp() {
    printf("\nplease input employee ID: ");
    int id1;
    scanf("%d",&id1);
    int ID = -1;
    for(int i=0;i< empCount; i++){
        // if find the employee
        if(emp[i].ID == id1){
          ID = i;
        }
    }
    
    if (ID >= 0) {
     printf("do you want to remove the following employee to the DB?\n");
          printf("      %s", emp[ID].fName);
          printf(" %s", emp[ID].lName);
          printf(", salary: %d\n", emp[ID].salary);
          printf("Enter 1 for yes, 0 for no: ");
          int choice;
          scanf("%d",&choice);
          // put the information into database
          if(choice == 1){
           for(int i = ID; i < empCount - 1; i++) {
             emp[i] = emp[i + 1];
            }
           empCount--;
         }

    }
    else {
        printf("can not find the employee.");
    }

    
}


// the menu for updating information
void updateMenu() {
  printf("\n(1) update the ID");
printf("\n(2) update the last name");
printf ("\n (3) update the first name");
printf ("\n (4) update the salary");
printf ("\n (5) done for the updating\n");
   
}














// update the information of user, according to the input number,
// we can update salary, last name, first name and ID

void update(int ID) {
      updateMenu();
    int choice2 =  readChoice();
    while (choice2 != 5){
        if(choice2 == 1 ) {
            int newID;
           printf("\nPlease enter the new ID with six digits: ");
           scanf("%d",&newID);
           emp[ID].ID = newID;
  
        } else if (choice2 == 2) {
            char lastName[MAXNAME];
           printf("\nPlease enter the last name: ");
           scanf("%s",emp[ID].lName);
           
            
           
        } else if (choice2 == 3) {
           char firstName[MAXNAME];
           printf("\nPlease enter the first name: ");
           scanf("%s",emp[ID].fName);
           
            
        } else if (choice2 == 4) {
            int salary_new;
           printf("Enter employee's salary (30000 to 150000): ");
           scanf("%d",&salary_new);
           // if the user salry is not correct
          while(salary_new < 30000 || salary_new > 150000){
        printf("Invlaid Salary\n");
        printf("Enter employee's salary (30000 to 150000): ");
        scanf("%d",&salary_new);
    }
     emp[ID].salary = salary_new;
           
        } 
        updateMenu();
         choice2 = readChoice();
    }

}


// this function is used to update the employee information
void UpdateEmp() {
    printf("\nplease input employee ID: ");
    int id1;
    scanf("%d",&id1);
    int ID = -1;
    for(int i=0; i< empCount; i++){
        // if find the employee
        if(emp[i].ID == id1){
          ID = i;
        }
    }
    
    if (ID >= 0) {
        int choice;
     printf("do you want to update the following employee to the DB?\n");
          printf("      %s", emp[ID].fName);
          printf(" %s", emp[ID].lName);
          printf(", salary: %d\n", emp[ID].salary);
          printf("Enter 1 for yes, 0 for no: ");
          scanf("%d",&choice);
        if (choice = 1) {
            update(ID);
        }
         qsort(emp,  empCount, sizeof(Employee), compare);
}
else{
   printf("Employee with id %d not found in DB\n", id1); 
}

}


// the function to print the required highest salary
void printhighest() {
    int number;
    printf("Enter the number of Employee you want to see have the highest salary: ");
    scanf("%d",&number);
    if(number > empCount) {
        number = empCount;
    }
    if (number <= 0) {
        printf("wrong number.");
        return;
    }
     qsort(emp,  empCount, sizeof(Employee), compare_salary);
     printf("\n");
    printf("Fisrt_Name   Last_Name         SALARY             ID\n");
    printf("-----------------------------------------------------\n");
     for(int i  = 0; i < number; i++) {
      printf("%s", emp[i].fName);
      printf("     \t%s", emp[i].lName);
      printf("     \t%d", emp[i].salary);
       printf("     \t%d\n", emp[i].ID);    
     }
    printf("-----------------------------------------------------\n");
    printf("Number of Employees  (%d)\n", number);
    qsort(emp,  empCount, sizeof(Employee), compare);
}


// this function to find the employee with same last name and it is case
// insensitive
void findSameLastName() {
    char str[MAXNAME];
    char str1[MAXNAME];
     printf("please enter the last name: ");
    scanf("%s", str);
   
    for(int i = 0; str[i]; i++){
     str[i] = tolower(str[i]);
   }
   printf("\n");
    printf("Fisrt_Name   Last_Name         SALARY             ID\n");
    printf("-----------------------------------------------------\n");
    for (int i = 0; i < empCount; i++) {
        strcpy(str1, emp[i].lName);
        //printf("\n%s", str1);
        for(int j = 0; str1[j]; j++){
        str1[j] = tolower(str1[j]);
       }
       if(strcmp(str1,str) == 0) {
          printf("%s", emp[i].fName);
      printf("     \t%s", emp[i].lName);
      printf("     \t%d", emp[i].salary);
       printf("     \t%d\n", emp[i].ID);    
     } 
       }
       
       
       
    }



// read the input choice, it should be between 1 and 9
int readChoice2() {
    int choice;
     printf("Enter your choice: ");
     scanf("%d",&choice); 
     while( choice > 9 || choice < 0) {
        printf("Hey, %d is not between 1 and 5, try again...", choice);
        printf("Enter your choice: ");
        scanf("%d",&choice); 
     }
     return choice;
}



// this is the main oerator to deal with different command from user

void operation() {
      int choice;
      menu();
     choice = readChoice2();
       while (choice != 5){
        if(choice == 1 ) {
            printAll();
        } else if (choice == 2) {
            lookByID();
        } else if (choice == 3) {
            lookByLName();
        } else if (choice == 4) {
            addEmp();
        } 
        else if (choice == 6) {
          removeEmp();  
        }
        else if (choice == 7) {
            UpdateEmp();
        }
        else if (choice == 8) {
           printhighest(); 
        }
        else if (choice == 9) {
           findSameLastName(); 
        }
         menu();
         choice = readChoice2();
    }
    return;
}


int main()   
{
    char str[100];
    // ask the user to input the file name
    printf("./workersDB ");
    scanf("%s", str);
    FILE *in_file;
    int file_status;
    file_status = oepn_file(str, &in_file);
    // file can not be opened
    if (file_status == -1) {
        printf("can not open the file");
        return 0;
    }
    // read the data into array
    int ret = 0;
    while (ret != -1) {
        Employee empy;
        ret =  read_int(&empy.ID, in_file);
        ret = read_string(&*empy.fName, in_file);
        ret = read_string(&*empy.lName, in_file);
        ret = read_int(&empy.salary, in_file);
        if(ret != -1) {
        emp[empCount] = empy;
        empCount++;}
    } 
   
   // while(fscanf(in_file,"%d %s %s %df",&emp[empCount].ID, emp[empCount].fName, emp[empCount].lName, &emp[empCount].salary) != EOF){
   //     empCount++;
  //  }
    
     close_file(&in_file);
   qsort(emp,  empCount, sizeof(Employee), compare);
   operation();

     printf("goodbye!");

  




    return 0;
}
